package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class ArtworksList(
    val idList: Int,
    val idUser: Int,
    val name: String,
    val size: Int,
    val creationDate: String
)