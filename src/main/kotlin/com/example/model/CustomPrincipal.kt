package com.example.model

import com.example.database.repository.UserRepository
import io.ktor.server.auth.*
import java.security.MessageDigest
import kotlin.text.Charsets.UTF_8

data class CustomPrincipal(val userName: String, val realm: String) : Principal
val userRepository = UserRepository()
fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"
val userTable: MutableMap<String, ByteArray> = mutableMapOf(
    "admin" to getMd5Digest("admin:$myRealm:admin")
)

fun usersMap(){
    val users = userRepository.selectAllUsers()
    for (user in users!!){
        if(!(userTable.containsKey(user.name))){
            userTable.put(user.name, getMd5Digest("${user.name}:${myRealm}:${user.password}"))
        }
    }
}