package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val idUser: Int,
    val name: String,
    val password: String,
)