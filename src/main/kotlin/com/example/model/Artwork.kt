package com.example.model

import kotlinx.serialization.Serializable
@Serializable
data class Artwork(
    var idArtwork: Int,
    var name: String,
    var artist: String,
    var description: String,
    var image: String,
    var year: Int,
    var location: String,
    var genres: MutableList<String>?,
    var idCreator: Int,
)