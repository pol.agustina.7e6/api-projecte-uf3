package com.example.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.imageRouting() {
    route("/images") {
        get("/{name}") {
            if (call.parameters["name"].isNullOrBlank()) return@get call.respondText(
                "Missing name of file",
                status = HttpStatusCode.BadRequest
            )
            val name = call.parameters["name"]!!
            val path = "src/main/kotlin/com/example/images/"+name
            call.respondFile(File(path))
        }
    }
}