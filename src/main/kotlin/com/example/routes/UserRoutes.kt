package com.example.routes

import com.example.database.repository.ArtworkRepository
import com.example.database.repository.ListRepository
import com.example.database.repository.UserRepository
import com.example.model.ArtworksList
import com.example.model.User
import com.example.model.usersMap
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.userRouting() {
    val repository = UserRepository()
    val repositoryLists = ListRepository()
    val repositoryArtwork = ArtworkRepository()
    route("/users") {
        authenticate("auth-digest"){
            get {
                val users = repository.selectAllUsers()
                if (!users.isNullOrEmpty()) call.respond(users)
                else call.respondText("No artworks found", status = HttpStatusCode.OK)
            }
            post("/login") {
                usersMap()
                val data = call.receive<User>()
                val user = repository.getUserByCredentials(data)
                if(user!=null) call.respond(user)
                else call.respondText("No user found",status = HttpStatusCode.NotFound)
            }
        }
        post {
            val user = call.receive<User>()
            if (repository.insertUser(user)) {
                usersMap()
                return@post call.respondText("User has been posted", status = HttpStatusCode.Accepted)
            } else{
                call.respondText("Could not post new user", status = HttpStatusCode.InternalServerError)
            }
        }
    }
    route("/users/{iduser?}"){
        authenticate ("auth-digest") {
            get() {
                if(call.parameters["iduser"].isNullOrBlank()) return@get call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val id = call.parameters["iduser"]!!
                val user = repository.selectUserById(id)
                if(user!=null) call.respond(user)
                else call.respondText("No user with id $id found",status = HttpStatusCode.NotFound)
            }
            get("/lists") {
                if(call.parameters["iduser"].isNullOrBlank()) return@get call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val lists = repositoryLists.selectUserLists(call.parameters["iduser"]!!)
                if(!lists.isNullOrEmpty()) call.respond(lists)
                else call.respondText("No lists found",status = HttpStatusCode.OK)
            }
            get("/posts") {
                if(call.parameters["iduser"].isNullOrBlank()) return@get call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val posts = repositoryArtwork.selectUserPosts(call.parameters["iduser"]!!.toInt())
                if(!posts.isNullOrEmpty()) call.respond(posts)
                else call.respondText("No lists found",status = HttpStatusCode.NotFound)
            }
        }
        put() {
            if(call.parameters["iduser"].isNullOrBlank()) return@put call.respondText("Missing id",status = HttpStatusCode.BadRequest)
            val user = call.receive<User>()
            val id = call.parameters["iduser"]!!
            if (repository.updateUser(id,user)) {
                return@put call.respondText("User has been updated", status = HttpStatusCode.Accepted)
            }
            call.respondText("Could not update user", status = HttpStatusCode.InternalServerError)
        }
        delete() {
            if(call.parameters["iduser"].isNullOrBlank()) return@delete call.respondText("Missing id",status = HttpStatusCode.BadRequest)
            val id = call.parameters["iduser"]!!
            if (repository.deleteUser(id)){
                return@delete call.respondText("User has been deleted",status = HttpStatusCode.Accepted)
            }
            call.respondText("Could not delete user", status = HttpStatusCode.InternalServerError)
        }
    }
}
