package com.example.routes

import com.example.database.repository.ArtworkRepository
import com.example.model.Artwork
import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException


fun Route.artworkRouting() {

    val repository = ArtworkRepository()

    route("/artworks") {
        get {
            val artworks = repository.selectAllArtworks()
            if (artworks.isNotEmpty()) call.respond(artworks)
            else call.respondText("No artworks found", status = HttpStatusCode.OK)
        }

        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]!!.toInt()
            val artwork = repository.selectArtworkById(id)
            if (artwork != null) call.respond(artwork)
            else call.respondText("No artwork with id $id", status = HttpStatusCode.NotFound)
        }

        post {
            val artworkData = call.receiveMultipart()
            var artwork = Artwork(1, "", "", "", "", 1, "", mutableListOf(),0)
            artworkData.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        if (part.name == "artwork") {
                            println(part.value)
                            val artworkString = part.value
                            artwork = Gson().fromJson(artworkString, Artwork::class.java)
                        }
                    }

                    is PartData.FileItem -> {
                        val routeImg = "images/" + (part.originalFileName as String)
                        artwork.image = routeImg
                        val fileName = "src/main/kotlin/com/example/images/" + (part.originalFileName as String)
                        val fileBytes = part.streamProvider().readBytes()
                        File(fileName).writeBytes(fileBytes)
                    }

                    else -> {}
                }
            }
            if (repository.insertArtwork(artwork)) {
                return@post call.respondText("Artwork has been posted", status = HttpStatusCode.Accepted)
            }
            call.respondText("Could not post new artwork", status = HttpStatusCode.InternalServerError)

        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]!!.toInt()

            val artworkData = call.receiveMultipart()
            val artwork = Artwork(id, "", "", "", "", 1, "", mutableListOf(),0)

            artworkData.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        when (part.name) {
                            "idArtwork" -> artwork.idArtwork = part.value.toInt()
                            "name" -> artwork.name = part.value
                            "artist" -> artwork.artist = part.value
                            "description" -> artwork.description = part.value
                            "year" -> artwork.year = part.value.toInt()
                            "location" -> artwork.location = part.value
                            "genres" -> artwork.genres = part.value as MutableList<String>
                        }
                    }

                    is PartData.FileItem -> {
                        artwork.image = "src/main/kotlin/com/example/images/" + (part.originalFileName as String)
                        val fileBytes = part.streamProvider().readBytes()
                        File(artwork.image!!).writeBytes(fileBytes)
                    }
                    else -> {}
                }
            }
            if (repository.updateArtwork(id.toString(), artwork)){
                return@put call.respondText("Artwork with id $id has been updated",status = HttpStatusCode.Accepted)
            }
            else call.respondText("Artwork could not be updated", status = HttpStatusCode.InternalServerError)
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]!!
            if(repository.deleteArtwork(id)){
                return@delete call.respondText("Artwork removed correctly",status = HttpStatusCode.Accepted)
            }
            else {
                call.respondText("Artwork with id $id could not be deleted", status = HttpStatusCode.InternalServerError)
            }
        }
    }
}


