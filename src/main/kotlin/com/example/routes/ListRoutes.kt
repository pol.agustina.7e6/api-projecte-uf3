package com.example.routes

import com.example.database.repository.ListRepository
import com.example.model.ArtworksList
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.listRouting() {

    val repository = ListRepository()

    route("/lists") {
        get {
            val lists = repository.selectAllLists()
            if(!lists.isNullOrEmpty()) call.respond(lists)
            else call.respondText("No lists found",status = HttpStatusCode.OK)
        }
        route("/{idlist?}") {
            get {
                if(call.parameters["idlist"].isNullOrBlank()) return@get call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val id = call.parameters["idlist"]!!
                val list = repository.selectListById(id)
                if(list!=null) call.respond(list)
                else call.respondText("Invalid id",status = HttpStatusCode.OK)
            }
            put() {
                if(call.parameters["idlist"].isNullOrBlank()) return@put call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val id = call.parameters["idlist"]!!
                val artworkList = call.receive<ArtworksList>()
                if (repository.updateList(id,artworkList)) {
                    return@put call.respondText("List with id $id has been updated",status = HttpStatusCode.Accepted)
                }
                call.respondText("List with id $id could not be updated", status = HttpStatusCode.NotFound)
            }
            delete() {
                if(call.parameters["idlist"].isNullOrBlank()) return@delete call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                val id = call.parameters["idlist"]!!
                if (repository.deleteList(id)) {
                    return@delete call.respondText("List with id $id has been deleted",status = HttpStatusCode.Accepted)
                }
                call.respondText("List with id $id could not be deleted", status = HttpStatusCode.NotFound)
            }
            route("/artworks") {
                get {
                    if(call.parameters["idlist"].isNullOrBlank()) return@get call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                    val artworks = repository.selectArtworksFromList(call.parameters["idlist"]!!)
                    if(!artworks.isNullOrEmpty()) call.respond(artworks)
                    else call.respondText("No artworks found",status = HttpStatusCode.NotFound)
                }
                delete("{idartwork?}") {
                    if(call.parameters["idartwork"].isNullOrBlank()) return@delete call.respondText("Missing id",status = HttpStatusCode.BadRequest)
                    val id = call.parameters["idartwork"]!!
                    if (repository.deleteArtworkFromList(call.parameters["idlist"]!!,id)){
                        return@delete call.respondText("Artwork with id $id has been deleted",status = HttpStatusCode.Accepted)
                    }
                    call.respondText("Artwork with id $id could not be deleted", status = HttpStatusCode.NotFound)
                }
                post {
                    val idArtwork = call.receive<Int>()
                    val listToAdd = repository.selectListById(call.parameters["idlist"]!!)
                    if (repository.addArtworkToList(listToAdd!!.idUser,listToAdd.idList,idArtwork)){
                        return@post call.respondText("Artwork has been posted",status = HttpStatusCode.Accepted)
                    }
                    else call.respondText("Artwork could not be posted", status = HttpStatusCode.InternalServerError)
                }
            }
        }
        post {
            val artworkList = call.receive<ArtworksList>()
            if (repository.insertList(artworkList)){
                return@post call.respondText("List has been posted",status = HttpStatusCode.Accepted)
            }
            else call.respondText("List could not be posted", status = HttpStatusCode.InternalServerError)
        }
    }
}
