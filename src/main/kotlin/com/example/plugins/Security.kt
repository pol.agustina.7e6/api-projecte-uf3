package com.example.plugins

import com.example.model.CustomPrincipal
import com.example.model.myRealm
import com.example.model.userTable
import io.ktor.server.auth.*
import io.ktor.server.application.*

fun Application.configureSecurity() {
    install(Authentication) {
        digest("auth-digest") {
            realm = myRealm
            digestProvider { userName, realm ->
                userTable[userName]
            }
            validate { credentials ->
                if (credentials.userName.isNotEmpty()) {
                    CustomPrincipal(credentials.userName, credentials.realm)
                } else {
                    null
                }
            }
        }
    }
}