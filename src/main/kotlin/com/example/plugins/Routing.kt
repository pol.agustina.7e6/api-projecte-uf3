package com.example.plugins

import com.example.routes.artworkRouting
import com.example.routes.imageRouting
import com.example.routes.listRouting
import com.example.routes.userRouting
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        artworkRouting()
        userRouting()
        listRouting()
        imageRouting()
    }
}
