package com.example.database.repository

import com.example.database.core.ConnectionHelper
import com.example.database.dao.ListDao
import com.example.model.Artwork
import com.example.model.ArtworksList
import com.example.model.User
import java.sql.SQLException

class ListRepository:ListDao {

    val connection = ConnectionHelper.getConnection()
    override fun selectAllLists(): MutableList<ArtworksList>? {
        val lists = mutableListOf<ArtworksList>()
        return try{
            val resultSet = connection!!.createStatement().executeQuery("SELECT * FROM artworks_lists")
            if (resultSet != null) {
                while (resultSet.next()){
                    lists.add(ArtworksList(
                        resultSet.getInt("id_artwork_list"),
                        resultSet.getInt("id_user"),
                        resultSet.getString("name"),
                        resultSet.getInt("size"),
                        resultSet.getString("creation_date")
                    ))
                }
                resultSet.close()
            }
            lists
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            null
        }
    }
    override fun selectUserLists(userId: String): MutableList<ArtworksList>? {
        val lists = mutableListOf<ArtworksList>()
        val query = "SELECT * FROM artworks_lists WHERE id_user=?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, userId.toInt())
            val resultSet = statement.executeQuery()
            while(resultSet.next()) {
                val artworksList = ArtworksList(
                    idList =  resultSet.getInt("id_artwork_list"),
                    idUser =  resultSet.getInt("id_user"),
                    name =  resultSet.getString("name"),
                    size =  resultSet.getInt("size"),
                    creationDate = resultSet.getString("creation_date"))
                lists.add(artworksList)
            }
            statement.close()
            lists
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            null
        }
    }

    override fun selectListById(listId: String): ArtworksList? {
        var artworksList: ArtworksList? = null
        val query = "SELECT * FROM artworks_lists WHERE id_artwork_list = $listId"
        try{
            val statement = connection!!.createStatement()
            val resultSet = statement.executeQuery(query)
            if (resultSet.next() != null) {
                artworksList = ArtworksList(
                    idList =  resultSet.getInt("id_artwork_list"),
                    idUser =  resultSet.getInt("id_user"),
                    name =  resultSet.getString("name"),
                    size =  resultSet.getInt("size"),
                    creationDate = resultSet.getString("creation_date"))
                resultSet.close()
            }
        }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}") }
        return artworksList
    }

    override fun selectArtworksFromList(listId: String): MutableList<Artwork>? {
        val artworks = mutableListOf<Artwork>()
        val query = "SELECT * FROM artworks WHERE id_artwork IN (SELECT id_artwork  FROM users_artworks_lists_items WHERE id_artwork_list=?);"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, listId.toInt())
            val resultSet = statement.executeQuery()
            while(resultSet.next()) {
                artworks.add(Artwork(
                    resultSet.getInt("id_artwork"),
                    resultSet.getString("name"),
                    resultSet.getString("artist"),
                    resultSet.getString("description"),
                    resultSet.getString("image"),
                    resultSet.getInt("year"),
                    resultSet.getString("location"),
                    (resultSet.getArray("genres").array as Array<*>).filterIsInstance<String>() as MutableList<String>,
                    resultSet.getInt("id_creator")
                ))
            }
            statement.close()
            artworks
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            null
        }
    }

    override fun addArtworkToList(userId:Int, listId: Int, artworkId: Int): Boolean {
        val query = "INSERT INTO users_artworks_lists_items (id_user, id_artwork_list, id_artwork) " +
                "SELECT ?, ?, ? " +
                "WHERE NOT EXISTS (" +
                "  SELECT 1 FROM users_artworks_lists_items " +
                "  WHERE id_artwork_list = ? AND id_artwork = ?" +
                ");"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, userId.toInt())
            statement.setInt(2, listId.toInt())
            statement.setInt(3, artworkId.toInt())
            statement.setInt(4, listId.toInt())
            statement.setInt(5, artworkId.toInt())
            statement.executeUpdate()
            statement.close()
            true
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun deleteArtworkFromList(listId: String, artworkId: String): Boolean {
        val query = "DELETE FROM users_artworks_lists_items WHERE id_artwork_list = ? AND id_artwork = ?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, listId.toInt())
            statement.setInt(2, artworkId.toInt())
            statement.executeUpdate()
            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        false
        }
    }

    override fun insertList(artworksList: ArtworksList): Boolean {
        val query = "INSERT INTO artworks_lists (id_user, name, size, creation_date) VALUES (?,?,?,?)"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, artworksList.idUser)
            statement.setString(2, artworksList.name)
            statement.setInt(3, artworksList.size)
            statement.setString(4, artworksList.creationDate)
            statement.executeUpdate()
            statement.close()
            true
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun updateList(id: String, artworksList: ArtworksList): Boolean {
        val query = "UPDATE artworks_lists SET name=?, size=?, creation_date=? WHERE id_artwork_list=?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setString(1, artworksList.name)
            statement.setInt(2, artworksList.size)
            statement.setString(3, artworksList.creationDate)
            statement.setInt(4, id.toInt())
            statement.executeUpdate()
            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun deleteList(id: String): Boolean {
        val query = "DELETE FROM artworks_lists WHERE id_artwork_list = ?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, id.toInt())
            statement.executeUpdate()
            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }
}