package com.example.database.repository

import com.example.database.core.ConnectionHelper
import com.example.database.dao.ArtworkDao
import com.example.model.Artwork
import com.example.model.ArtworksList
import java.sql.SQLException

class ArtworkRepository:ArtworkDao {

    val connection = ConnectionHelper.getConnection()

    override fun selectAllArtworks(): MutableList<Artwork> {
        val artworks = mutableListOf<Artwork>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM artworks")
        try{
            if (query != null) {
                while (query.next()){
                    artworks.add(Artwork(
                        query.getInt("id_artwork"),
                        query.getString("name"),
                        query.getString("artist"),
                        query.getString("description"),
                        query.getString("image"),
                        query.getInt("year"),
                        query.getString("location"),
                        (query.getArray("genres").array as Array<*>).filterIsInstance<String>() as MutableList<String>,
                        query.getInt("id_creator")
                    ))
                }
                query.close()
            }
        }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}")}
        return artworks
    }

    override fun selectUserPosts(userId: Int): MutableList<Artwork>? {
        val artworks = mutableListOf<Artwork>()
        val query = "SELECT * FROM artworks WHERE id_creator=$userId"
        try{
            val resultSet = connection!!.createStatement().executeQuery(query)
            if (resultSet != null) {
                while (resultSet.next()){
                    artworks.add(Artwork(
                        resultSet.getInt("id_artwork"),
                        resultSet.getString("name"),
                        resultSet.getString("artist"),
                        resultSet.getString("description"),
                        resultSet.getString("image"),
                        resultSet.getInt("year"),
                        resultSet.getString("location"),
                        (resultSet.getArray("genres").array as Array<*>).filterIsInstance<String>() as MutableList<String>,
                        resultSet.getInt("id_creator")
                    ))
                }
                resultSet.close()
            }
        }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}")}
        return artworks
    }

    override fun selectArtworkById(id: Int): Artwork? {
        var artwork:Artwork? = null
        val query = "SELECT * FROM artworks WHERE id_artwork = $id"
        try{
            val statement = connection!!.createStatement()
            val resultSet = statement.executeQuery(query)
            if (resultSet.next() != null) {
                    artwork = Artwork(
                        resultSet.getInt("id_artwork"),
                        resultSet.getString("name"),
                        resultSet.getString("artist"),
                        resultSet.getString("description"),
                        resultSet.getString("image"),
                        resultSet.getInt("year"),
                        resultSet.getString("location"),
                        (resultSet.getArray("genres").array as Array<*>).filterIsInstance<String>() as MutableList<String>,
                        resultSet.getInt("id_creator"),
                    )
                }
                resultSet.close()
            }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}") }
        return artwork
    }

    override fun insertArtwork(artwork: Artwork):Boolean {
        val query = "INSERT INTO artworks (name, artist, description, image, year, location, genres, id_creator) VALUES (?,?,?,?,?,?,?,?)"

        return try {
            val statement = connection!!.prepareStatement(query)

            statement.setString(1, artwork.name)
            statement.setString(2, artwork.artist)
            statement.setString(3, artwork.description)
            statement.setString(4, artwork.image)
            statement.setInt(5, artwork.year)
            statement.setString(6, artwork.location)
            val genres = connection.createArrayOf("VARCHAR", artwork.genres!!.toTypedArray())
            statement.setArray(7, genres)
            statement.setInt(8, artwork.idCreator)
            statement.executeUpdate()

            statement.close()
            true
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun updateArtwork(id: String, artwork: Artwork):Boolean {
        val query = "UPDATE artworks SET name=?, artist=?, description=?, image=?, year=?, location=?, genres=? WHERE id_artwork=?"
        return try {
            val statement = connection!!.prepareStatement(query)

            statement.setString(1, artwork.name)
            statement.setString(2, artwork.artist)
            statement.setString(3, artwork.description)
            statement.setString(4, artwork.image)
            statement.setInt(5, artwork.year)
            statement.setString(6, artwork.location)
            statement.setArray(7, artwork.genres as java.sql.Array)
            statement.setInt(8, id.toInt())

            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }

    }

    override fun deleteArtwork(id: String):Boolean {
        val query = "DELETE FROM artworks WHERE id_artwork = ?"
        return try {
            val statement = connection!!.prepareStatement(query)

            statement.setInt(1, id.toInt())
            statement.executeUpdate()

            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }
}