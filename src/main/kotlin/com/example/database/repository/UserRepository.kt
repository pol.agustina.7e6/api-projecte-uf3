package com.example.database.repository

import com.example.database.core.ConnectionHelper
import com.example.database.dao.UserDao
import com.example.model.User
import java.sql.SQLException
import java.sql.SQLIntegrityConstraintViolationException

class UserRepository:UserDao {

    val connection = ConnectionHelper.getConnection()

    override fun selectAllUsers(): MutableList<User>? {
        val users = mutableListOf<User>()
        return try{
            val query = connection!!.createStatement().executeQuery("SELECT * FROM users")
            if (query != null) {
                while (query.next()){
                    users.add(User(
                        query.getInt("id_user"),
                        query.getString("name"),
                        query.getString("password")
                    ))
                }
                query.close()
            }
            users
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            null
        }
    }

    fun getUserByCredentials(user: User): User? {
        var response: User? = null
        val query = "SELECT * FROM users WHERE name=? AND password=?"
        try {
            val statement = connection!!.prepareStatement(query)
            statement.setString(1, user!!.name)
            statement.setString(2, user!!.password)
            val resultSet = statement.executeQuery()
            if (resultSet.next() != null) {
                response = User(
                    resultSet.getInt("id_user"),
                    resultSet.getString("name"),
                    resultSet.getString("password"))
                resultSet.close()
            }
        }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}") }
        return response
    }
    override fun selectUserById(id: String): User? {
        var user: User? = null
        val query = "SELECT * FROM users WHERE id_user = $id"
        try{
            val statement = connection!!.createStatement()
            val resultSet = statement.executeQuery(query)
            if (resultSet.next() != null) {
                user = User(
                    resultSet.getInt("id_user"),
                    resultSet.getString("name"),
                    resultSet.getString("password"))
                resultSet.close()
            }
        }catch (e: SQLException) { println("Error ${e.errorCode}: ${e.message}") }
        return user
    }

    override fun updateUser(id: String, user: User): Boolean {
        val query = "UPDATE users SET name=?, password=? WHERE id_user = ?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setString(1, user.name)
            statement.setString(2, user.password)
            statement.setInt(3, id.toInt())
            statement.executeUpdate()
            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun insertUser(user: User): Boolean {
        val query = "INSERT INTO users (name, password) VALUES (?,?)"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setString(1, user.name)
            statement.setString(2, user.password)
            statement.executeUpdate()
            statement.close()
            true
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }

    override fun deleteUser(id: String): Boolean {
        val query = "DELETE FROM users WHERE id_user = ?"
        return try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, id.toInt())
            statement.executeUpdate()
            statement.close()
            true
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
            false
        }
    }
}