package com.example.database.dao

import com.example.model.User

interface UserDao {
    fun selectAllUsers(): MutableList<User>?
    fun selectUserById( id: String): User?
    fun insertUser(user: User): Boolean
    fun updateUser(id: String, user: User): Boolean
    fun deleteUser(id: String): Boolean
}