package com.example.database.dao

import com.example.model.Artwork

interface ArtworkDao {
    fun selectAllArtworks(): MutableList<Artwork>
    fun selectUserPosts(userId: Int): MutableList<Artwork>?
    fun selectArtworkById(id: Int): Artwork?
    fun insertArtwork(artwork: Artwork): Boolean
    fun updateArtwork(id: String, artwork: Artwork): Boolean
    fun deleteArtwork(id: String): Boolean
}