package com.example.database.dao

import com.example.model.Artwork
import com.example.model.ArtworksList

interface ListDao {
    fun selectAllLists(): MutableList<ArtworksList>?
    fun selectUserLists(userId: String): MutableList<ArtworksList>?
    fun selectListById(listId: String): ArtworksList?
    fun selectArtworksFromList(listId:String): MutableList<Artwork>?
    fun addArtworkToList(userId:Int, listId:Int, artworkId:Int): Boolean
    fun deleteArtworkFromList(listId:String, artworkId:String): Boolean
    fun insertList(artworksList: ArtworksList): Boolean
    fun updateList(id: String, artworksList: ArtworksList): Boolean
    fun deleteList(id: String): Boolean
}