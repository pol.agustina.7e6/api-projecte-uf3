package com.example.database.core
import java.sql.*


object ConnectionHelper {
    fun getConnection() : Connection? {
        val jdbcUrl = "jdbc:postgresql://localhost:5432/artgallerydb"
        val username = "postgres"
        val password = "postgres"
        var connection: Connection? = null
        try {
            connection = DriverManager.getConnection(jdbcUrl, username, password)
        } catch (e: SQLException) {
            println("Artgallery ha experimentat un error intern: ${e.errorCode} ${e.message}")
        }
        return connection
    }
}