CREATE DATABASE artgallerydb;

\c artgallerydb

CREATE TABLE users (
    id_user SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE artworks (
    id_artwork SERIAL PRIMARY KEY,
    name VARCHAR(255),
    artist VARCHAR(255),
    description TEXT,
    image VARCHAR(255),
    year INT,
    location VARCHAR(255),
    genres VARCHAR(255)[],
    id_creator INT NOT NULL,
    FOREIGN KEY (id_creator) REFERENCES users (id_user)
    ON DELETE CASCADE
);

CREATE TABLE artworks_lists (
    id_artwork_list SERIAL PRIMARY KEY,
    id_user INT NOT NULL,
    name VARCHAR(255),
    size INT,
    creation_date VARCHAR(255),
    CONSTRAINT fk_user
    FOREIGN KEY (id_user) REFERENCES users (id_user)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE users_artworks_lists_items (
    id_user_artwork_list_item SERIAL PRIMARY KEY,
    id_artwork INT NOT NULL,
    id_artwork_list INT NOT NULL,
    id_user INT NOT NULL,
    CONSTRAINT fk_artwork
    FOREIGN KEY (id_artwork) REFERENCES artworks (id_artwork)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_artwork_list
    FOREIGN KEY (id_artwork_list) REFERENCES artworks_lists (id_artwork_list)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_user
    FOREIGN KEY (id_user) REFERENCES users (id_user)
    ON UPDATE CASCADE ON DELETE CASCADE
);

--GUIAS INSERTS
INSERT INTO users (name, password) VALUES ('admin', 'admin'), ('Da Vinci', 'pasta'), ('Vincent','oreja'), ('Dali','bigote');
INSERT INTO artworks (name, artist, description, image, year, location, genres, id_creator) VALUES (
'La noche estrellada',
'Vincent van Gogh',
'La noche estrellada representa la vista desde la ventana orientada al este de su habitacion de asilo en Saint-Remy-de-Provence, justo antes del amanecer, con la adicion de un pueblo imaginario.',
'images/nocheestrellada.jpg',
1889,
'Museo de Arte Moderno, Nueva York',
ARRAY ['oleo', 'postimpresionismo'],
1
);

INSERT INTO artworks (name, artist, description, image, year, location, genres, id_creator) VALUES (
'La Mona Lisa',
'Leonardo Da Vinci',
'El retrato de Lisa Gherardini, esposa de Francesco del Giocondo, más conocido como La Gioconda o Monna Lisa, es una obra pictórica del polímata renacentista italiano Leonardo da Vinci.',
'images/monalisa.jpg',
1503,
'Museo del Louvre, Francia',
ARRAY ['oleo', 'renacimiento'],
1
);

INSERT INTO artworks (name, artist, description, image, year, location, genres, id_creator) VALUES (
'El nacimiento de Venus',
'Sandro Botticelli',
'El nacimiento de Venus es un cuadro realizado por el pintor renacentista Sandro Botticelli, una de las obras cumbre del maestro florentino y del Quattrocento italiano.',
'images/nacimientodevenus.jpg',
1485,
'Galeria degli Uffizi, Italia',
ARRAY ['temple ', 'renacimiento'],
1
);


INSERT INTO artworks_lists (id_user, name, size, creation_date) VALUES (
1,
'Favourites',
2,
'22-04-2023'
);
INSERT INTO users_artworks_lists_items (id_artwork, id_artwork_list, id_user) VALUES (1,1,1), (1,1,2);
SELECT * FROM artworks WHERE id_artwork IN (SELECT id_artwork  FROM users_artworks_lists_items WHERE id_user = 1 AND id_artwork_list=4);
